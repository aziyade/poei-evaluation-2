create table IF NOT EXISTS members (
                         id integer generated always as identity primary key,
                         pseudonym text,
                         firstname text,
                         lastname text
);


create table IF NOT EXISTS wines (
                       id integer generated always as identity primary key ,
                       appellation text,
                       region text,
                       colour text,
                       vintage integer

);

CREATE TABLE IF NOT EXISTS reviews
(
    id        integer primary key generated always as identity primary key,
    member_id integer references members (id),
    wine_id   integer references wines (id),
    score     integer

)




