package com.zenika.poei.amismaisonduvin.repository.member;

import com.zenika.poei.amismaisonduvin.domain.Member;
import org.springframework.jdbc.core.RowMapper;


import java.sql.ResultSet;
import java.sql.SQLException;


public class MemberRowMapper implements RowMapper<Member> {

    @Override
    public Member mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Member(rs.getInt("id"), rs.getString("pseudonym"), rs.getString("firstname"), rs.getString("lastname"));
    }
}

