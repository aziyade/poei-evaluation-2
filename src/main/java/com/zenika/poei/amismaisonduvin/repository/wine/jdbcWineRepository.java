package com.zenika.poei.amismaisonduvin.repository.wine;

import com.zenika.poei.amismaisonduvin.domain.Wine;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.time.Year;
import java.util.List;


@Component

public class jdbcWineRepository implements WineRepository {

    //TODO verifier les info à retourner en fct de la consigne

    private final WineRowMapper ROW_MAPPER = new WineRowMapper();
    private final JdbcTemplate jdbcTemplate;

    public jdbcWineRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public List<Wine> getAll() {

        return jdbcTemplate.query("SELECT * FROM wines", ROW_MAPPER);
    }


    @Override
    public Wine getById(int id) {

        return jdbcTemplate.queryForObject(
                "select * from wines where id =? ",
                ROW_MAPPER,
                id);
    }


    @Override
    public Wine getByRegion(String region) {

        return jdbcTemplate.queryForObject(
                "select * from wines where region = ? ",
                ROW_MAPPER,
                region);
    }


    @Override
    public Wine save(Wine wine) {

        return jdbcTemplate.queryForObject("insert into wines(appellation, region, colour, vintage) values (? , ?, ?, ?) returning *", ROW_MAPPER,
                wine.getAppellation(), wine.getRegion(), wine.getColour(), wine.getVintage());
    }


    @Override
    public Wine delete(Wine wine) {
        jdbcTemplate.update("delete from wines where appellation = ?",
                wine.getAppellation());
        return wine;
    }
}
