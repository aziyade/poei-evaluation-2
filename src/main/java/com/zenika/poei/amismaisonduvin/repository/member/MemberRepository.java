package com.zenika.poei.amismaisonduvin.repository.member;

import com.zenika.poei.amismaisonduvin.domain.Member;

import java.util.List;

public interface MemberRepository {


    List<Member> getAll();

    Member getById(int id);

    Member save(Member member);

    Member update(Integer id, Member member);


}


