package com.zenika.poei.amismaisonduvin.repository.member;


import com.zenika.poei.amismaisonduvin.domain.Member;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class jdbcMemberRepository implements MemberRepository {
    private final MemberRowMapper ROW_MAPPER = new MemberRowMapper();
    private final JdbcTemplate jdbcTemplate;

    public jdbcMemberRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Member> getAll() {
        return jdbcTemplate.query("select * from members",
                ROW_MAPPER);
    }


    @Override
    public Member getById(int id) {
        return jdbcTemplate.queryForObject(
                "select * from members where id = ?", ROW_MAPPER, id);

    }

    @Override
    public Member save(Member member) {
        return jdbcTemplate.queryForObject("insert into members(pseudonym, firstname, lastname) values (?,?,?) returning *", ROW_MAPPER,
                member.getPseudonym(), member.getFirstName(), member.getLastName());
    }

    @Override
    public Member update(Integer id, Member member) {
        jdbcTemplate.update("delete from members where id =?", id);
        return save(member);
    }
}



