package com.zenika.poei.amismaisonduvin.repository.wine;

import com.zenika.poei.amismaisonduvin.domain.Wine;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WineRowMapper implements RowMapper<Wine> {

    @Override
    public Wine mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Wine(rs.getInt("id"), rs.getString("appellation")
                , rs.getString("region"), rs.getString("colour"),
                rs.getInt("vintage"));
    }

}

