package com.zenika.poei.amismaisonduvin.repository.wine;

import com.zenika.poei.amismaisonduvin.domain.Wine;

import java.time.Year;
import java.util.List;

public interface WineRepository {

    List<Wine> getAll();

    Wine getById(int id);

    Wine getByRegion(String region);

    Wine save(Wine wine);

    Wine delete(Wine wine);
}

