package com.zenika.poei.amismaisonduvin.controller;


import com.zenika.poei.amismaisonduvin.domain.Wine;
import com.zenika.poei.amismaisonduvin.repository.wine.WineRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WineController {

    private final WineRepository wineRepository;

    public WineController(WineRepository wineRepository) {

        this.wineRepository = wineRepository;
    }


    // recuperer la liste des vins
    @GetMapping("/wines")
    public List<Wine> getWines() {
        return wineRepository.getAll();
    }


    // récuoérer un vin par son id
    @GetMapping("/wines/{id}") // taper l'id dans l'url = retourne le produit correspondant
    public Wine getWineById(@PathVariable int id) {
        return wineRepository.getById(id);
    }


    // récuoérer un vin par sa région
    @GetMapping("/regions/{region}/wines") // taper la region dans l'url = retourne le produit correspondant
    public Wine getWineByRegion(@PathVariable String region) {
        return wineRepository.getByRegion(region);
    }


    //sauvegarder un vin
    @PostMapping( "/wines")
    public Wine saveWine(@RequestBody Wine wine) {
        return wineRepository.save(wine);
    }


    //supprimer un vin
    @DeleteMapping("/wines/{id}")
    public Wine deleteWineById(@PathVariable int id) {
        Wine wine = wineRepository.getById(id);
        wineRepository.delete(wine);
        return wine;

    }
}
