package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.repository.member.MemberRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MemberController {

    private final MemberRepository memberRepository;


    public MemberController(MemberRepository memberRepository) {

        this.memberRepository = memberRepository;
    }


    //recuperer tout les membres
    @GetMapping("/members")
    public List<Member> getMembers() {

        return memberRepository.getAll();
    }

    //recuperer un membre avec son id
    @GetMapping("/members/{id}")
    public Member getMembreById(@PathVariable int id) {
        return memberRepository.getById(id);
    }

    // sauvegarder un utilisateur
    @PostMapping(value = "/members")
    public Member saveMember(@RequestBody Member member) {
        return memberRepository.save(member);
    }


    //mettre à jour un utilisateur
    @PutMapping("/members/{id}")
    public Member updateMember(@RequestParam Integer id, @RequestBody Member member) {
        return memberRepository.update(id, member);

    }


}
